module dtree;

public import dparser;
public import std.variant;
public import std.traits;

/* struct DTree {

    JSONValue value;

    private void assign(T)(T arg) {
        static if(is(T : string)) {
            value = parseJSON(arg);
        } else {
            value = arg;
        }
    }
    
    this(T)(T arg){
        assign(arg);
    }

    DTree opCall(T)(T arg){
        assign(arg);
        return this;
    }

    DTree opCall(){
        return this;
    }

    void opAssign(T)(T arg){
        assign(arg);
    }
    
    JSONValue opIndex(string path){
        auto keys = split(path, ".");
        return get(keys);
    }

    JSONValue get(ref JSONValue json_value,  string[] keys){
        auto *temp = value;
        foreach (key in keys){
            temp = &temp[key]
        }
    }
} */

enum DType : string {
    
    Null    = "Null",    /// Indicates the type of a $(D TreeValue).
    Bool    = "Bool",    /// ditto
    Long    = "Long",    /// ditto
    Ulong   = "Ulong",   /// ditto
    Double  = "Double",  /// ditto
    String  = "String",  /// ditto
    Array   = "Array",   /// ditto
    Object  = "Object"   /// ditto
}

struct DTree {

    import std.stdio, std.typecons;

    private DValue _value;
    private DType _type;
    
    this(T)(T arg){
        //assign(parse(arg))
        assign(arg);
    }
    
    DTree opCall(string arg){
        //assign(parse(arg))
        assign(arg);
        return this;
    }

    DTree opCall(){
        assign(null);
        return this;
    }

    void opAssign(T)(T arg){
        assign(arg);
    }
    
    @property DType type() const pure nothrow @safe @nogc {
        return _type;
    }
    
    @property DValue value() const pure nothrow @safe @nogc {
        return _value;
    }

    @property DValue value(T)(T arg) {
        assign(arg);
        return _value;
    }

    private void assign(T)(T arg) {
        static if(is(T : typeof(null))){
            _type = DType.Null;
            _value = DValue();
        } else static if(is(T : string)){
            _type = DType.String;
            _value = arg;
        } else static if(is(T : bool)){
            _type = DType.Bool;
            _value = arg;
        } else static if(is(T : ulong) && isUnsigned!T){
            _type = DType.Ulong;
            _value = cast(ulong)arg;
        } else static if(is(T : long)){
            _type = DType.Long;
            _value = cast(long)arg;
        } else static if(isFloatingPoint!T) {
            _type = DType.Double;
            _value = cast(double)arg;
        } else static if(is(T : Value[Key], Key, Value)) {
            static assert(is(Key : string), "AA key must be string");
            _type = DType.Object;
            static if(is(Value : DTree)) {
                _value = arg;
            } else {
                DTree[string] tree;
                foreach(key, val; arg)
                    tree[key] = DTree(val);
                _value = tree;
            }
        } else static if(isArray!T) {
            _type = DType.Array;
            static if(is(ElementEncodingType!T : DTree)) {
                _value = arg;
            } else {
                DTree[] arr = new DTree[arg.length];
                foreach(i, e; arg) {
                    arr[i] = DTree(e);
                }
                _value = arr;
            }
        } else static if(is(T : DTree)) {
            _type = arg.type;
            _value = arg.value;
        } else {
            static assert(false, text(`unable to convert type "`, T.stringof, `" to DTree`));
        }
            
    }
    
    private void assignRef(T)(ref T arg) if(isStaticArray!T) {
        _type = DTree.Array;
        static if(is(ElementEncodingType!T : DTree)) {
            _value = arg;
        } else {
            DTree[] arr = new DTree[arg.length];
            foreach(i, e; arg)
                arr[i] = DTree(e);
            _value = arr;
        }
    }
    
}


class DTreeException : Exception {
    this(string msg, int line = 0, int pos = 0) pure nothrow @safe {
        if(line) {
            super(text(msg, " (Line ", line, ":", pos, ")"));
        } else {
            super(msg);
        }
    }

    this(string msg, string file, size_t line) pure nothrow @safe {
        super(msg, file, line);
    }
}