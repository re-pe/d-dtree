module dparser;

public import dvalue;

enum DOptions {
    none,                       /// standard parsing
    specialFloatLiterals = 0x1, /// encode NaN and Inf float values as strings
}

struct DParser {
    private DValue function(string str) _parse;
    private int _maxDepth = -1;
    private DOptions _options = DOptions.none;
    
    this(DValue function(string str) parseFunc, int maxDepth = -1, DOptions options = DOptions.none){
        _parse = parseFunc;
        if (maxDepth > -1){
            _maxDepth = maxDepth;
        }
        if (options > DOptions.none) {
            _options = options;
        }
    }
}

