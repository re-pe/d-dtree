module dvalue;

alias DValue = Algebraic!(bool, long, ulong, double, string, DTree[], DTree[string]);