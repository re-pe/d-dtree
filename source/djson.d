module djson;

public import dparser;
public import dtree;

enum JSONFloatLiteral : string
{
    nan         = "NaN",       /// string representation of floating-point NaN
    inf         = "Infinite",  /// string representation of floating-point Infinity
    negativeInf = "-Infinite", /// string representation of floating-point negative Infinity
}

DParser JSONParser(){
    return DParser(DValue function(string str){
        
    });
}